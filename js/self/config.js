var b = [];
var fileCount;
var animStatus = false;
var i = 0;
var numClickEcam = 0;
var numClickPfd = 0;
var buttonClickStatus = 0;

var timeVal = 125;

var temp1 = [];					//Engine ESD Page N2 Left
var temp1_pos = '-3.4 4 -5'
var temp1_col = '#804600'
var temp1_size = '5'

var temp2 = [];					//Engine ESD Page N2 Right
var temp2_pos = '-1.8 4 -5'
var temp2_col = 'green'
var temp2_size = '5'

var temp3 = [];					//Engine ESD Page N3 Left
var temp3_pos = '1.6 4 -5'
var temp3_col = 'green'
var temp3_size = '5'

var temp4 = [];					//Engine ESD Page N3 Right
var temp4_pos = '2 4 -5'
var temp4_col = 'green'
var temp4_size = '5'

var temp5 = [];					//Engine ESD Page FF Left
var temp5_pos = '0.8 4 -5'
var temp5_col = 'green'
var temp5_size = '5'

var temp6 = [];					//Engine ESD Page FF Right
var temp6_pos = '-0.35 3.7 -5'
var temp6_col = 'green'
var temp6_size = '5'

var temp7 = [];					//Engine ESD Page QT Left
var temp7_pos = '0 4 -5'
var temp7_col = 'green'
var temp7_size = '5'

var temp8 = [];					//Engine ESD Page QT Right
var temp8_pos = '-3.1 2.335 -5'
var temp8_col = 'green'
var temp8_size = '5'

var temp9 = [];					//Engine ESD Page C Left
var temp9_pos = '2.453 2.307 -5'
var temp9_col = 'green'
var temp9_size = '5'

var temp10 = [];				//Engine ESD Page C Right
var temp10_pos = '-0.902 2.859 -5'
var temp10_col = 'green'
var temp10_size = '4'

var temp11 = [];				//Engine ESD Page PSI Left
var temp11_pos = '1.023 2.859 -5'
var temp11_col = 'green'
var temp11_size = '4'

var temp12 = [];				//Engine ESD Page PSI Right
var temp12_pos = '1 -3 -5'
var temp12_col = 'green'
var temp12_size = '5'

var temp13 = [];				//Engine ESD Page N1 Left
var temp13_pos = '-1 -4 -5'
var temp13_col = 'green'
var temp13_size = '5'

var temp14 = [];				//Engine ESD Page N1 Right
var temp14_pos = '-1 2.372 -5'
var temp14_col = 'green'
var temp14_size = '5'

var temp15 = [];				//Engine ESD Page N2 Left
var temp15_pos = '0.5 2.372 -5'
var temp15_col = 'green'
var temp15_size = '5'

var temp16 = [];				//Engine ESD Page N2 Right
var temp16_pos = '1 -5 -5'
var temp16_col = 'green'
var temp16_size = '5'

var temp17 = [];				//Engine ESD Page N3 Left
var temp17_pos = '-1 -6 -5'
var temp17_col = 'green'
var temp17_size = '5'

var temp18 = [];				//Engine ESD Page N3 Right
var temp18_pos = '1 -6 -5'
var temp18_col = 'green'
var temp18_size = '5'

var ecamESDMne = ['AFLOOR', 'TRM', 'NAI1', 'WAI1AC', 'PACKS', 'THRIDLE', 'THRL1', 'EGT1', 'EGT2', 'THRA1', 'THRA2', 'THRC1', 'THRC2', 'N1A1', 'N1A2', 'TTRND1', 'TTRND2', 'temp18'];
var ecamESDVal = [temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, temp10, temp11, temp12, temp13, temp14, temp15, temp16, temp17, temp18];

var temp19 = [];				//Engine PD Page TAT
var temp19_pos = '-5.7 -3.635 -5'
var temp19_col = 'green'
var temp19_size = '5'

var temp20 = [];				//Engine PD Page SAT
var temp20_pos = '-5.7 -3.835 -5'
var temp20_col = 'green'
var temp20_size = '5'

var temp21 = [];				//Engine PD Page ISA
var temp21_pos = '-5.7 -4.035 -5'
var temp21_col = 'green'
var temp21_size = '5'

var temp22 = [];				//Engine PD Page TIME
var temp22_pos = '-1.242 -3.635 -5'
var temp22_col = 'green'
var temp22_size = '5'

var temp23 = [];				//Engine PD Page GW
var temp23_pos = '-1.242 -3.835 -5'
var temp23_col = 'green'
var temp23_size = '5'

var temp24 = [];				//Engine PD Page GW/CG
var temp24_pos = '-1.242 -4.035 -5'
var temp24_col = 'green'
var temp24_size = '5'

var temp25 = [];				//Engine PD Page FOB
var temp25_pos = '-3.65 -4 -5'
var temp25_col = 'green'
var temp25_size = '5'

var ecamPDMne = ['CTATSAT', 'SAT1', 'ISAC', 'GWK', 'CG', 'TFOB', 'UTC'];
var ecamPDVal = [temp19, temp20, temp21, temp22, temp23, temp24, temp25];

var temp26 = [];				//Engine SD Page FF Left
var temp26_pos = '1.375 1 -5'
var temp26_col = 'green'
var temp26_size = '5'

var temp27 = [];				//Engine SD Page FF Right
var temp27_pos = '3.725 1 -5'
var temp27_col = 'green'
var temp27_size = '5'

var temp28 = [];				//Engine SD Page FU Left
var temp28_pos = '3.875 -0.1 -5'
var temp28_col = 'green'
var temp28_size = '4'

var temp29 = [];				//Engine SD Page FU Right
var temp29_pos = '3 -2 -5'
var temp29_col = 'green'
var temp29_size = '5'

var temp30 = [];				//Engine SD Page Block
var temp30_pos = '5.575 -1.745 -5'
var temp30_col = 'green'
var temp30_size = '5'

var temp31 = [];				//Engine SD Page FU Total
var temp31_pos = '0.828 -2 -5'
var temp31_col = 'green'
var temp31_size = '5'

var temp32 = [];				//Engine SD Page Block - FU Total
var temp32_pos = '1.375 0.5 -5'
var temp32_col = 'green'
var temp32_size = '5'

var temp33 = [];				//Engine SD Page LDG Elevn
var temp33_pos = '3.727 0.5 -5'
var temp33_col = 'green'
var temp33_size = '5'

var temp34 = [];				//Engine SD Page CAB ALT
var temp34_pos = '1 0.5 -5'
var temp34_col = 'green'
var temp34_size = '5'

var temp35 = [];				//Engine SD Page CAB V/S
var temp35_pos = '-1 -5 -5'
var temp35_col = 'green'
var temp35_size = '5'

var temp36 = [];				//Engine SD Page Delta P PSI
var temp36_pos = '-1 -5 -5'
var temp36_col = 'green'
var temp36_size = '5'

var temp37 = [];				//Engine SD Page Delta P FT
var temp37_pos = '-1 -6 -5'
var temp37_col = 'green'
var temp37_size = '5'

var temp38 = [];				//Engine SD Page Delta P FT/Min
var temp38_pos = '1 -6 -5'
var temp38_col = 'green'
var temp38_size = '5'

var temp39 = [];				//Engine SD Page Plane1
var temp39_pos = '1 -6 -5'
var temp39_col = 'green'
var temp39_size = '5'

var temp40 = [];				//Engine SD Page Plane2
var temp40_pos = '1 -2 -5'
var temp40_col = 'green'
var temp40_size = '5'

var temp41 = [];				//Engine SD Page Plane3
var temp41_pos = '-1 -3 -5'
var temp41_col = 'green'
var temp41_size = '5'

var temp42 = [];				//Engine SD Page Plane4
var temp42_pos = '1 -3 -5'
var temp42_col = 'green'
var temp42_size = '5'

var temp43 = [];				//Engine SD Page Plane5
var temp43_pos = '-1 -4 -5'
var temp43_col = 'green'
var temp43_size = '5'

var temp44 = [];				//Engine SD Page Plane6
var temp44_pos = '1 -4 -5'
var temp44_col = 'green'
var temp44_size = '5'

var ecamSDMne = ['FFKG1', 'FFKG2', 'FU', 'CABALT', 'CABVS', 'DELTAP', 'FU1', 'FU2', 'temp34', 'temp35', 'temp36', 'temp37', 'temp38', 'temp39', 'temp40', 'temp41', 'temp42', 'temp43', 'temp44']
var ecamSDVal = [temp26, temp27, temp28, temp29, temp30, temp31, temp32, temp33, temp34, temp35, temp36, temp37, temp38, temp39, temp40, temp41, temp42, temp43, temp44]

/* ------*********------ */

var temp51 = [];
var temp51_pos = '-0.4 0.526 0';
var temp51_col = 'white';
var temp51_size = '1';

var temp52 = [];
var temp52_pos = '-0.4 0.475 0';
var temp52_col = 'white';
var temp52_size = '1';

var temp53 = [];
var temp53_pos = '-0.4 0.434 0';
var temp53_col = 'white';
var temp53_size = '1';

var temp54 = [];
var temp54_pos = '-0.16 0.526 0';
var temp54_col = '#01ff02';
var temp54_size = '1';

var temp55 = [];
var temp55_pos = '-0.16 0.475 0';
var temp55_col = 'blue';
var temp55_size = '1';

var temp56 = [];
var temp56_pos = '0.065 0.526 0';
var temp56_col = '#01ff02';
var temp56_size = '1';

var temp58 = [];
var temp58_pos = '0.065 0.475 0';
var temp58_col = 'blue';
var temp58_size = '1';

var temp59 = [];
var temp59_pos = '0.485 0.526 0';
var temp59_col = 'white';
var temp59_size = '1';

var temp60 = [];
var temp60_pos = '0.485 0.475 0';
var temp60_col = 'white';
var temp60_size = '1';

var temp61 = [];
var temp61_pos = '0.485 0.434 0';
var temp61_col = 'white';
var temp61_size = '1';

var pfdHDMne = ['ATSMODES', 'ATHRMESS', 'FMALONG', 'FMALONGARMED', 'FMALAT', 'FMALATARMED', 'APSTATUS', 'FDSTATUS', 'ATSSTATUS'];
var pfdHDVal = [temp51, temp53, temp54, temp55, temp56, temp58, temp59, temp60, temp61];

var temp62 = [];
var temp63 = [];
var temp98 = [];
var temp99 = [];
var temp95 = [];
var temp96 = [];

var pfdVSCptMne = ['ALTRCPT', 'RAPFDCPT','ALTCPT','ALTBAROCCPT','SALTFCU'];
var pfdVSCptVal = [temp62, temp63, temp98, temp99, temp96];

var temp65 = [];
var temp66 = [];
var temp67 = [];

var pfdLAWMne = ['ALAW', 'DLAW', 'NLAW'];
var pfdLAWVal = [temp65, temp66, temp67];

var temp64 = [];
var temp68 = [];
var temp69 = [];
var temp70 = [];
var temp71 = [];
var temp79 = [];
var temp86 = [];

var pfdATCptMne = ['ROLLCPT','YAWFD1','STKPCPT','STKRCPT','SSLIPCPT','SSLIPBCPT','PTCHCPT'];
var pfdATCptVal = [temp64, temp68, temp69, temp70, temp71, temp79, temp86];

var temp72 = [];
var temp72_pos = '-0.473 0 0.48';
var temp72_col = 'green';
var temp72_size = '1.3';

var temp73 = [];
var temp74 = [];
var temp75 = [];
var temp76 = [];
var temp77 = [];
var temp78 = [];
var temp84 = [];
var temp85 = [];
var temp87 = [];
var temp88 = [];
var temp92 = [];
var temp93 = [];
var temp94 = [];
var temp95 = [];
var temp97 = [];

var pfdHSMne = ['MACHCPT','STDCCPT','QNHCCPT','QFECCPT','HPACCPT','SBAROCHGCPT','SBAROCMBCPT','CASCPT','SPDTREND','SPDTGTCPT','SPDAUTO','VMAX','VLS','VALPHAPROT','VALPHAMAX','VALPHASTALLW'];
var pfdHSVal = [temp72, temp73, temp74, temp75, temp76, temp77, temp78, temp84, temp85, temp87, temp88, temp92, temp93, temp94, temp95, temp97];

var temp80 = [];
var temp81 = [];
var temp82 = [];

var pfdPTCPTMne = ['FDBARDISP','PTCHFD1','ROLLFD1'];
var pfdPTCPTVal = [temp80, temp81, temp82];

var temp83 = [];
var temp89 = [];
var temp90 = [];
var temp91 = [];

var pfdCMPCTPMne = ['HDGSCPT','MHDGCPT','SHDGFCU','MTRCKCPT'];
var pfdCMPCPTVal = [temp83, temp89, temp90, temp91];

/* ------*********------ */

var temp151 = [];
var temp151_pos = '-0.4 0.526 0';
var temp151_col = 'white';
var temp151_size = '1';

var temp152 = [];
var temp152_pos = '-0.4 0.475 0';
var temp152_col = 'white';
var temp152_size = '1';

var temp153 = [];
var temp153_pos = '-0.4 0.434 0';
var temp153_col = 'white';
var temp153_size = '1';

var temp154 = [];
var temp154_pos = '-0.16 0.526 0';
var temp154_col = '#01ff02';
var temp154_size = '1';

var temp155 = [];
var temp155_pos = '-0.16 0.475 0';
var temp155_col = 'blue';
var temp155_size = '1';

var temp156 = [];
var temp156_pos = '0.065 0.526 0';
var temp156_col = '#01ff02';
var temp156_size = '1';

var temp158 = [];
var temp158_pos = '0.065 0.475 0';
var temp158_col = 'blue';
var temp158_size = '1';

var temp159 = [];
var temp159_pos = '0.485 0.526 0';
var temp159_col = 'white';
var temp159_size = '1';

var temp160 = [];
var temp160_pos = '0.485 0.475 0';
var temp160_col = 'white';
var temp160_size = '1';

var temp161 = [];
var temp161_pos = '0.485 0.434 0';
var temp161_col = 'white';
var temp161_size = '1';

var pfdHDFOMne = ['ATSMODES', 'ATHRMESS', 'FMALONG', 'FMALONGARMED', 'FMALAT', 'FMALATARMED', 'APSTATUS', 'FDSTATUS', 'ATSSTATUS'];
var pfdHDFOVal = [temp151, temp153, temp154, temp155, temp156, temp158, temp159, temp160, temp161];

var temp162 = [];
var temp163 = [];
var temp198 = [];
var temp199 = [];
var temp195 = [];
var temp196 = [];

var pfdVSFOMne = ['ALTRFO', 'RAPFDFO','ALTFO','ALTBAROCFO','SALTFCU'];
var pfdVSFOVal = [temp162, temp163, temp198, temp199, temp196];

var temp165 = [];
var temp166 = [];
var temp167 = [];

var pfdLAWFOMne = ['ALAW', 'DLAW', 'NLAW'];
var pfdLAWFOVal = [temp165, temp166, temp167];

var temp164 = [];
var temp168 = [];
var temp169 = [];
var temp170 = [];
var temp171 = [];
var temp179 = [];
var temp186 = [];

var pfdATFOMne = ['ROLLFO','YAWFD2','STKPFO','STKRFO','SSLIPFO','SSLIPIRSFO','PTCHFO'];
var pfdATFOVal = [temp164, temp168, temp169, temp170, temp171, temp179, temp186];

var temp172 = [];
var temp172_pos = '-0.473 0 0.48';
var temp172_col = 'green';
var temp172_size = '1.3';

var temp173 = [];
var temp174 = [];
var temp175 = [];
var temp176 = [];
var temp177 = [];
var temp178 = [];
var temp184 = [];
var temp185 = [];
var temp187 = [];
var temp188 = [];
var temp192 = [];
var temp193 = [];
var temp194 = [];
var temp195 = [];
var temp197 = [];

var pfdHSFOMne = ['MACHFO','STDFFO','QNHFFO','QFEFFO','HPAFFO','SBAROFHGFO','SBAROFMBFO','CASFO','SPDTREND','SPDTGTFO','SPDAUTO','VMAX','VLS','VALPHAPROT','VALPHAMAX','VALPHASTALLW'];
var pfdHSFOVal = [temp172, temp173, temp174, temp175, temp176, temp177, temp178, temp184, temp185, temp187, temp188, temp192, temp193, temp194, temp195, temp197];

var temp180 = [];
var temp181 = [];
var temp182 = [];

var pfdPTFOMne = ['FDBARDISP','PTCHFD2','ROLLFD2'];
var pfdPTFOVal = [temp180, temp181, temp182];

var temp183 = [];
var temp189 = [];
var temp190 = [];
var temp191 = [];

var pfdCMPFOMne = ['HDGSFO','MHDGFO','SHDGFCU','MTRCKFO'];
var pfdCMPFOVal = [temp183, temp189, temp190, temp191];


















