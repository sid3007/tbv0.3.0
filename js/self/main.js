window.onload = function()
{
	var url = document.location.href,
	params = url.split('?')[1].split('&'),
	data = {}, tmp;
	for (var i = 0, l = params.length; i < l; i++) 
	{
	tmp = params[i].split('=');
	data[tmp[0]] = tmp[1];
	}
	console.log(data.name);
	var url = "./files/" + data.name;
	readTextFile(url);

	THREE.Cache.clear();
	AFRAME.scenes[0].systems.material.clearTextureCache();
}

function readTextFile(file)
{
  	var rawFile = new XMLHttpRequest();
  	rawFile.open("GET", file, false);
  	rawFile.onreadystatechange = function ()
  	{
    	if(rawFile.readyState === 4)
    	{
      		if(rawFile.status === 200 || rawFile.status == 0)
      		{
        		var allText = rawFile.responseText.split(/\r\n|\n/);
        		for(var i = 0; i < allText.length; i++)
	            {
	              	var c = allText[i];
	              	b.push(c.split(','));
	            }
      		}
    	}
  	}
  	rawFile.send(null);
  	fileCount = b.length;
  	lineCount = b[0].length;
  	ecamESDVisual();
  	ecamPDVisual();
  	ecamSDVisual();

  	pfdHDVisual();
  	pfdVSCptVisual();
  	pfdATCptVisual();
  	pfdLAWVisual();
  	pfdHSVisual();
  	pfdPTCPTVisual();
	pfdCMTCPTVisual();
	  
	// pfdHDFOVisual();
	// pfdVSFOVisual();
  	// pfdATFOVisual();
  	// pfdLAWFOVisual();
  	// pfdHSFOVisual();
  	// pfdPTFOVisual();
	// pfdCMTFOVisual();
}

function ecamSDVisual()
{
	for(var i = 0; i < ecamSDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamSDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamSDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function ecamESDVisual()
{
	for(var i = 0; i < ecamESDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamESDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamESDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function ecamPDVisual()
{
	for(var i = 0; i < ecamPDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case ecamPDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					ecamPDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdHDVisual()
{
	for(var i = 0; i < pfdHDMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdHDMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdHDVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdVSCptVisual()
{
	for(var i = 0; i < pfdVSCptMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdVSCptMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdVSCptVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdATCptVisual()
{
	for(var i = 0; i < pfdATCptMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdATCptMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdATCptVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdLAWVisual()
{
	for(var i = 0; i < pfdLAWMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdLAWMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdLAWVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdHSVisual()
{
	for(var i = 0; i < pfdHSMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdHSMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdHSVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdPTCPTVisual()
{
	for(var i = 0; i < pfdPTCPTMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdPTCPTMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdPTCPTVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function pfdCMTCPTVisual()
{
	for(var i = 0; i < pfdCMPCTPMne.length; i++)
	{
		for(var j = 0; j < lineCount; j++)
		{
			switch(b[0][j])
			{
				case pfdCMPCTPMne[i]: 
				for(var k = 1; k < fileCount; k++)
				{
					pfdCMPCPTVal[i].push(b[k][j]);
				}
				break;
			}
		}
	}
}

function textChange(ob, pos, col, text, size, val)
{
	if(text != undefined)
	{
		ob.setAttribute('position',pos);
		ob.setAttribute('color',col);
		ob.setAttribute('width',size);

		if(val === 0 || val === '0')
		{
			ob.setAttribute('value','');
		}

		else
		{
			ob.setAttribute('value', text);	
		}
	}
}

function animationPause(ob, animBegin, animPause, animResume)
{
	var currentEvent = animBegin;
	document.getElementById(ob).dispatchEvent(new CustomEvent(currentEvent))
  	switch(currentEvent)
  	{
	    case animBegin:
	    case animResume:
	      	currentEvent = animPause
	    break
	    case animPause:
	      	currentEvent = animResume          
	    break  
  	}
}

function changeAnimationParameters(ob, from, to)
{
	document.getElementById(ob).setAttribute('animation','from',from);
	document.getElementById(ob).setAttribute('animation','to',to);
}

function elemCreate(file, id, pos, rot, sca, parent, kind)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement(kind);
	scene.appendChild(entity);
	entity.setAttribute('position', pos);
	entity.setAttribute('rotation', rot);
	entity.setAttribute('scale', sca);
	entity.setAttribute('id', id);
	entity.setAttribute('fbx-model', 'src', file);
}

function entityCreate(id, pos, rot, sca, parent, kind)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement(kind);
	scene.appendChild(entity);
	entity.setAttribute('position', pos);
	entity.setAttribute('rotation', rot);
	entity.setAttribute('scale', sca);
	entity.setAttribute('id', id);
}

function imgCreate(file, id, pos, rot, sca, parent, kind, width, height, vis, tar)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement(kind);
	scene.appendChild(entity);
	entity.setAttribute('position', pos);
	entity.setAttribute('rotation', rot);
	entity.setAttribute('scale', sca);
	entity.setAttribute('id', id);
	entity.setAttribute('src', file);
	entity.setAttribute('width', width);
	entity.setAttribute('height', height);
	entity.setAttribute('visible', vis);
	entity.setAttribute('clip', tar);
}

function planeCreate(id, cla, geo, rot, pos, parent, kind, vis)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement(kind);
	scene.appendChild(entity);
	entity.setAttribute('position', pos);
	entity.setAttribute('rotation', rot);
	entity.setAttribute('id', id);
	entity.setAttribute('class', cla);
	entity.setAttribute('geometry', geo);
	entity.setAttribute('visible', vis);
}

function textCreate(id, value, color, width, position, parent, font, neg)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement('a-text');
	scene.appendChild(entity);
	entity.setAttribute('position', position);
	entity.setAttribute('id', id);
	entity.setAttribute('color', color);
	entity.setAttribute('width', width);
	entity.setAttribute('value', value);
	entity.setAttribute('font', font);
	entity.setAttribute('negate', neg);
}

function boxCreate(id, color, pos, rot, sca, vis, parent)
{
	var scene = document.getElementById(parent);
	var entity = document.createElement('a-box');
	scene.appendChild(entity);
	entity.setAttribute('position', pos);
	entity.setAttribute('rotation', rot);
	entity.setAttribute('scale', sca);
	entity.setAttribute('id', id);
	entity.setAttribute('color', color);
	entity.setAttribute('visible', vis);
}

function callPfd()
{
	if(numClickPfd === 0)
	{
		numClickPfd++;

		elemCreate('models/pfd/angle.fbx', 'stModel2', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/mask.fbx', 'stModel3', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/pointer1.fbx', 'stModel4', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/reference.fbx', 'stModel5', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/ruler.fbx', 'stModel6', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/new.fbx', 'stModel7', '0 0 0', '0 0 0', '1 0.09 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/new2.fbx', 'stModel8', '-0.02 -0.024 0.066', '0 0 0', '1 1 1', 'pfd', 'a-entity');
		elemCreate('models/pfd/sidestick.fbx', 'stModel9', '0 0 0', '0 0 0', '1 1 1', 'pfd', 'a-entity');

		elemCreate('models/pfd/pointer2.fbx', 'rollPoint', '0 0 0', '0 0 0', '1 1 1', 'attitude', 'a-entity');
		elemCreate('models/pfd/pointer4.fbx', 'rollTrapezoid', '0 0 0', '0 0 0', '1 1 1', 'attitude', 'a-entity');
		elemCreate('models/pfd/pointer4.fbx', 'cyanTrapezoid', '0 0 0', '0 0 0', '1 1 1', 'attitude', 'a-entity');
		
		elemCreate('models/pfd/order.fbx', 'order', '0 0 0', '0 0 0', '1 1 1', 'attitude', 'a-entity');
		elemCreate('models/pfd/yaw.fbx', 'yaw', '0 0 0', '0 0 0', '1 1 1', 'attitude', 'a-entity');
		
		imgCreate('models/pfd/equal.png', 'bankProt1', '-491.283 0 -285.376', '90 -15 0', '0.5 0.5 0.5', 'bankAngleProtectionEqual', 'a-image', '78', '41', 'true', '0');
		imgCreate('models/pfd/equal.png', 'bankProt2', '493.548 0 -285.376', '90 15 0', '0.5 0.5 0.5', 'bankAngleProtectionEqual', 'a-image', '78', '41', 'true', '0');
		
		imgCreate('models/pfd/cross.png', 'bankProt3', '-491.283 0 -285.376', '90 -15 0', '0.5 0.5 0.5', 'bankAngleProtectionCross', 'a-image', '62', '64', 'true', '0');
		imgCreate('models/pfd/cross.png', 'bankProt4', '493.548 0 -285.376', '90 15 0', '0.5 0.5 0.5', 'bankAngleProtectionCross', 'a-image', '62', '64', 'true', '0');

		boxCreate('rollBar', '#2975e6', '0 0 0', '0 0 0', '0.01 0.01 0.3', 'false', 'attitude');
		boxCreate('pitchBar', '#2975e6', '0 0 0', '0 0 0', '0.01 0.01 0.3', 'false', 'attitude');

		imgCreate('models/pfd/center/cross.png', 'pitchCross', '0 0 0', '-90 0 0', '0.00065 0.00065 0.00065', 'attitude', 'a-image', '506', '1688', 'false', 'target: .cutPitch;');
		imgCreate('models/pfd/sky.png', 'pcrosssky', '0 -50 -20', '0 0 0', '1 1.3 1', 'pitchCross', 'a-image', '1024', '1024', 'true', 'target: .cutHorizon;');

		imgCreate('models/pfd/center/equal.png', 'pitchEqual', '0 0 0', '-90 0 0', '0.00065 0.00065 0.00065', 'attitude', 'a-image', '506', '1688', 'true', 'target: .cutPitch;');
		imgCreate('models/pfd/sky.png', 'pcrosssky2', '0 -50 -20', '0 0 0', '1 1.3 1', 'pitchEqual', 'a-image', '1024', '1024', 'true', 'target: .cutHorizon;');

		planeCreate('planepitch1', 'cutPitch', 'primitive:plane', '0 0 0', '0 0 -0.192', 'attitude', 'a-entity', 'false');
		planeCreate('planepitch2', 'cutPitch', 'primitive:plane', '0 180 0', '0 0 0.2', 'attitude', 'a-entity', 'false');

		planeCreate('planepitch3', 'cutHorizon', 'primitive:plane', '0 0 0', '0 0 -0.565', 'attitude', 'a-entity', 'false');
		planeCreate('planepitch4', 'cutHorizon', 'primitive:plane', '0 180 0', '0 0 0.6', 'attitude', 'a-entity', 'false');

		planeCreate('planepitch5', 'cutPitch', 'primitive:plane', '0 90 0', '-0.25 0 -0.134', 'attitude', 'a-entity', 'false');
		planeCreate('planepitch6', 'cutPitch', 'primitive:plane', '0 -90 0', '0.25 0 -0.134', 'attitude', 'a-entity', 'false');

		imgCreate('models/pfd/whiteline.png', 'noid1', '-0.225 0.505 0.025', '0 0 0', '0.00035 0.00035 0.00035', 'headerTexts', 'a-image', '14', '384', 'true', '0');
		imgCreate('models/pfd/whiteline.png', 'noid2', '0 0.505 0.025', '0 0 0', '0.00035 0.00035 0.00035', 'headerTexts', 'a-image', '14', '384', 'true', '0');
		imgCreate('models/pfd/whiteline.png', 'noid3', '0.225 0.505 0.025', '0 0 0', '0.00035 0.00035 0.00035', 'headerTexts', 'a-image', '14', '384', 'true', '0');
		imgCreate('models/pfd/whiteline.png', 'noid4', '0.43 0.505 0.025', '0 0 0', '0.00035 0.00035 0.00035', 'headerTexts', 'a-image', '14', '384', 'true', '0');

		textCreate('val1PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val2PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val3PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val4PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val5PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val6PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val8PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val9PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val10PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val11PFDHD', '0', 'green', '5', '0 0 -10', 'headerTexts', 'Arial_round_bold-msdf.json', 'false');

		imgCreate('models/pfd/vspeed.png', 'noidspeed1', '0.595 0.001 0', '-90 0 0', '0.00065 0.00065 0.00065', 'verticalSpeed', 'a-image', '80', '1299', 'true', '0');
		elemCreate('models/pfd/line/green.fbx', 'greenVSpeed', '0.595 0.001 0', '0 15 0', '0.01 0.002 0.002', 'verticalSpeed', 'a-entity');
		elemCreate('models/pfd/line/amber.fbx', 'amberVSpeed', '0.595 0.001 0', '0 15 0', '0.01 0.002 0.002', 'verticalSpeed', 'a-entity');

		textCreate('raHeight', '0', 'green', '3', '0 0 0', 'verticalSpeed', 'Arial_round_bold-msdf.json', 'false');
		raHeight.setAttribute('visible','false');
		raHeight.setAttribute('rotation', '-90 0 0');

		textCreate('digTextBox', '20', '#01ff02', '0.4', '0.595 0.02 0', 'verticalSpeed', 'Arial_round_bold-msdf.json', 'false');
		digTextBox.setAttribute('scale','1.7 1.7 1.7');
		digTextBox.setAttribute('rotation','-90 0 0');
		digTextBox.setAttribute('visible','false');
		boxCreate('noidbox1', 'black', '0.012 0 0', '0 0 0', '0.03 0.02 0.001', 'true', 'digTextBox');

		elemCreate('models/pfd/meter/metergreen.fbx', 'meterAmber', '0.065 -0.019 -0.005', '0 0 0', '0.025 0.025 0.025', 'verticalSpeed', 'a-entity');
		elemCreate('models/pfd/meter/meteramber.fbx', 'meterGreen', '0.065 -0.019 -0.005', '0 0 0', '0.025 0.025 0.025', 'verticalSpeed', 'a-entity');

		textCreate('vDigSpeed', '0', '#01ff02', '1', '0.365 0.033 -0.027', 'verticalSpeed', 'Arial_round_bold-msdf.json', 'false');
		vDigSpeed.setAttribute('rotation', '-90 0 0');
		imgCreate('models/pfd/altitude.png', 'vSpeedScale', '0.395 0.003 -9.75', '-90 0 0', '0.00065 0.00065 0.00065', 'verticalSpeed', 'a-image', '167', '33058', 'true', 'target: .cutHSpeed;');

		imgCreate('models/pfd/value.png', 'vSpeedHelp', '0.475 0.03 0', '-90 0 0', '0.0006 0.001 0.0005', 'verticalSpeed', 'a-image', '79', '283', 'true', 'target: .cutVSpeed;');
		imgCreate('models/pfd/value.png', 'noidspeed2', '0 -279.806 0', '0 0 0', '1 1 1', 'vSpeedHelp', 'a-image', '79', '283', 'true', 'target: .cutVSpeed;');
		imgCreate('models/pfd/value.png', 'noidspeed3', '0 270.555 0', '0 0 0', '1 1 1', 'vSpeedHelp', 'a-image', '79', '283', 'true', 'target: .cutVSpeed;');

		planeCreate('noidbox2', 'cutVSpeed', 'primitive:plane;', '0 0 0', '0 0.006 -0.1', 'verticalSpeed', 'a-entity', 'false');
		planeCreate('noidbox3', 'cutVSpeed', 'primitive:plane;', '0 180 0', '0 -0.002 0.05', 'verticalSpeed', 'a-entity', 'false');

		imgCreate('models/pfd/box.png', 'boxHelp', '0.338 0.030 -0.27', '-90 90 0', '0.0007 0.0007 0.0007', 'verticalSpeed', 'a-image', '188', '89', 'false', '0');
		boxCreate('noidbox4', 'black', '0 -99.777 0', '0 0 0', '50 200 1', 'true', 'boxHelp');
		textCreate('boxHelpText', '0', 'blue', '5', '0 0 0', 'boxHelp', 'Arial_round_bold-msdf.json', 'false');
		boxHelpText.setAttribute('rotation', '0 0 -90');
		boxHelpText.setAttribute('scale', '200 200 1000');

		textCreate('boxHelpUp', '0', 'blue', '1', '0.327 0 -0.374', 'verticalSpeed', 'Arial_round_bold-msdf.json', 'false');
		textCreate('boxHelpDown', '0', 'blue', '1', '0.327 0 0.409', 'verticalSpeed', 'Arial_round_bold-msdf.json', 'false');
		boxHelpUp.setAttribute('rotation', '-90 0 0');
		boxHelpDown.setAttribute('rotation', '-90 0 0');

		imgCreate('models/pfd/refline.png', 'noidimage1', '-0.367 0.003 0', '-90 0 0', '0.00065 0.00065 0.00065', 'horizontalSpeed', 'a-image', '113', '48', 'true', '0');

		imgCreate('models/pfd/speed_1.png', 'hSpeed', '-0.42 0.001 -1.775', '-90 0 0', '0.0006 0.0006 0.0006', 'horizontalSpeed', 'a-image', '233', '7542', 'true', 'target: .cutHSpeed;');
		imgCreate('models/pfd/speed_2.png', 'noidiamge2', '0 7540 0', '0 0 0', '1 1 1', 'hSpeed', 'a-image', '233', '7542', 'true', 'target: .cutHSpeed;');

		planeCreate('noidplane1', 'cutHSpeed', 'primitive:plane;', '0 0 0', '0 0 -0.34', 'horizontalSpeed', 'a-entity', 'false');
		planeCreate('noidplane2', 'cutHSpeed', 'primitive:plane;', '0 180 0', '0 0 0.38', 'horizontalSpeed', 'a-entity', 'false');

		textCreate('machN', '0', 'blue', '5', '0 0 -10', 'horizontalSpeed', 'Arial_round_bold-msdf.json', 'false');
		textCreate('sqfe', 'QNH', 'white', '0.9', '-0.518 0 0.494', 'horizontalSpeed', 'Arial_round_bold-msdf.json', 'false');
		sqfe.setAttribute('visible','false');
		textCreate('sqfeVal', '1102', 'blue', '0.9', '-0.427 0 0.494', 'horizontalSpeed', 'Arial_round_bold-msdf.json', 'false');
		sqfeVal.setAttribute('visible','false');
		machN.setAttribute('rotation', '-90 0 0');
		sqfe.setAttribute('rotation', '-90 0 0');
		sqfeVal.setAttribute('rotation', '-90 0 0');

		imgCreate('models/pfd/std.png', 'std', '-0.418 0.006 0.495', '-90 0 0', '0.00065 0.00065 0.00065', 'horizontalSpeed', 'a-image', '188', '89', 'true', '0');

		imgCreate('models/pfd/arrow.png', 'arrowTrend', '0 0.326 -8', '-90 0 0', '0.05 0.05 0.05', 'lineTrend', 'a-image', '63', '38', 'true', '0');
		lineTrend.setAttribute('visible','true');

		imgCreate('models/pfd/targetmagenta.png', 'targetM', '-0.335 0.007 -0.325', '-90 90 0', '0.00065 0.00065 0.00065', 'horizontalSpeed', 'a-image', '85', '85', 'true', '0');
		imgCreate('models/pfd/targetcyan.png', 'targetC', '-0.335 0.007 -0.325', '-90 90 0', '0.00065 0.00065 0.00065', 'horizontalSpeed', 'a-image', '85', '85', 'true', '0');	
		textCreate('targetUp', '0', 'green', '1.3', '-0.475 0.006 -0.369', 'horizontalSpeed', 'Arial_round_bold-msdf.json', 'false');
		textCreate('targetDown', '0', 'green', '1.3', '-0.475 0.006 0.409', 'horizontalSpeed', 'Arial_round_bold-msdf.json', 'false');
		targetUp.setAttribute('rotation','-90 0 0');
		targetDown.setAttribute('rotation','-90 0 0');

		imgCreate('models/pfd/vmax.png', 'vmaxRed', '-0.342 0.005 0.53', '90 0 0', '0.002 0.0085 0.001', 'horizontalSpeed', 'a-image', '6', '84', 'false', 'target: .cutHSpeed;');
		imgCreate('models/pfd/vmax.png', 'vminRed', '-0.342 0.005 0.53', '90 0 0', '0.002 0.0085 0.001', 'horizontalSpeed', 'a-image', '6', '84', 'false', 'target: .cutHSpeed;');
		imgCreate('models/pfd/vmin.png', 'vminAmber', '-0.342 0.005 0.75', '90 0 0', '0.002 0.0085 0.001', 'horizontalSpeed', 'a-image', '6', '84', 'false', 'target: .cutHSpeed;');
		imgCreate('models/pfd/red_line.png', 'vminRedLine', '-0.342 0.005 0.75', '90 0 0', '0.002 0.0085 0.001', 'horizontalSpeed', 'a-image', '6', '84', 'false', 'target: .cutHSpeed;');
		imgCreate('models/pfd/amber_line.png', 'vminAmberLine', '-0.342 0.005 0.75', '90 0 0', '0.002 0.0085 0.001', 'horizontalSpeed', 'a-image', '6', '84', 'false', 'target: .cutHSpeed;');

		imgCreate('models/pfd/equal.png', 'vProtEqual', '-0.369 0.003 -0.319', '90 0 0', '0.0004 0.0004 0.00065', 'horizontalSpeed', 'a-image', '78', '41', 'true', '0');
		imgCreate('models/pfd/cross.png', 'vProtCross', '-0.369 0.003 -0.319', '90 0 0', '0.0004 0.0004 0.00065', 'horizontalSpeed', 'a-image', '78', '41', 'true', '0');

		imgCreate('models/pfd/true.png', 'trueCom', '0 0.007 0.4', '-90 0 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '188', '80', 'false', '0');
		imgCreate('models/pfd/mag.png', 'magCom', '0 0.007 0.4', '-90 0 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '188', '80', 'false', '0');
		imgCreate('models/pfd/hdg.png', 'hdgCom', '0 0.007 0.4', '-90 0 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '188', '80', 'true', '0');

		imgCreate('models/pfd/compass.png', 'compassFoot', '0 -0.018 0.5', '-90 0 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '5981', '108', 'true', 'target: .cutcompass;');
		imgCreate('models/pfd/compass.png', 'noidaea2', '-5946.589 0 0', '-90 0 0', '0.00065 0.00065 0.00065', 'compassFoot', 'a-image', '5981', '108', 'true', 'target: .cutcompass;');

		imgCreate('models/pfd/targetcyan.png', 'compassTarget', '0 0.007 0.455', '-90 180 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '85', '85', 'true', 'target: .cutcompass;');
		textCreate('compassTargetLeft', '0', '#228cff', '1', '-0.308 0 0.434', 'footer', 'Arial_round_bold-msdf.json', 'false');
		textCreate('compassTargetRight', '0', '#228cff', '1', '0.173 0 0.434', 'footer', 'Arial_round_bold-msdf.json', 'false');
		compassTargetLeft.setAttribute('rotation', '-90 0 0');
		compassTargetRight.setAttribute('rotation', '-90 0 0');
		imgCreate('models/pfd/current.png', 'compassCurrent', '0 0.007 0.482', '-90 180 0', '0.00065 0.00065 0.00065', 'footer', 'a-image', '47', '72', 'true', 'target: .cutcompass;');
	}
}

function callEcam()
{
	if(numClickEcam === 0)
	{
		numClickEcam++;
		elemCreate('models/ecam/ecam.fbx', 'ecamBase', '3.478 0.410 -5', '90 0 0', '6.2 6.2 6.2', 'ecam', 'a-entity');

		imgCreate('models/ecam/circle.png', 'thrust1', '-1.064 3.192 -4.980', '0 0 0', '0.0053 0.0053 0.0053', 'ecam', 'a-image', '175', '175', 'false', 'target: .cutplane');
		elemCreate('models/ecam/line.fbx', 'thra1', '0 0 0', '0 90 90', '1.5 1.5 1.5', 'thrust1', 'a-entity');
		elemCreate('models/ecam/line.fbx', 'thrc1', '0 0 0', '0 90 90', '1.5 1.5 1.5', 'thrust1', 'a-entity');
		planeCreate('thra1plane1', 'cutplane', 'primitive:plane', '-45 90 0', '-5 0 0', 'thrust1', 'a-entity', 'false');
		planeCreate('thra1plane2', 'cutplane', 'primitive:plane', '-45 270 0', '0 0 0', 'thrust1', 'a-entity', 'false');

		imgCreate('models/ecam/circle.png', 'thrust2', '0.829 3.192 -4.980', '0 0 0', '0.0053 0.0053 0.0053', 'ecam', 'a-image', '175', '175', 'false', 'target: .cutim');
		elemCreate('models/ecam/line.fbx', 'thra2', '0 0 0', '0 90 90', '1.5 1.5 1.5', 'thrust2', 'a-entity');
		elemCreate('models/ecam/line.fbx', 'thrc2', '0 0 0', '0 90 90', '1.5 1.5 1.5', 'thrust2', 'a-entity');
		planeCreate('thra2plane1', 'cutim', 'primitive:plane', '-45 90 0', '-5 0 0', 'thrust2', 'a-entity', 'false');
		planeCreate('thra2plane2', 'cutim', 'primitive:plane', '-45 270 0', '0 0 0', 'thrust2', 'a-entity', 'false');

		elemCreate('0', 'needles', '0 0 0', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		elemCreate('models/ecam/needle.fbx', 'delP', '-2.315 -1.013 -5', '0 90 0', '0.008 0.008 0.008', 'needles', 'a-entity');
		elemCreate('models/ecam/needle.fbx', 'cabAlt', '-0.085 -1.097 -5', '0 90 0', '0.008 0.008 0.008', 'needles', 'a-entity');
		elemCreate('models/ecam/needle.fbx', 'cabVs', '2.406 -0.991 -5', '0 90 0', '0.008 0.008 0.008', 'needles', 'a-entity');
		elemCreate('models/ecam/needle.fbx', 'egtL', '-2.849 2.512 -5', '0 90 0', '0.007 0.007 0.007', 'needles', 'a-entity');
		elemCreate('models/ecam/needle.fbx', 'egtR', '2.679 2.452 -5', '0 90 0', '0.007 0.007 0.007', 'needles', 'a-entity');

		elemCreate('0', 'ecamSystemDisplay', '-3 0.6 0', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		textCreate('stText1', '20000', 'green', '4', '1.85 -0.1 -5', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText2', '18002', 'green', '4', '5.15 -0.1 -5', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText3', '25', 'green', '4', '3.109 0.192 -5', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');

		textCreate('val1SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val2SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val3SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val4SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val5SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val6SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val7SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val8SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val10SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val11SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val12SD', '0', 'green', '5', '0 0 -10', 'ecamSystemDisplay', 'Arial_round_bold-msdf.json', 'false');

		elemCreate('0', 'planeValues', '0 0 0', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		textCreate('stText4', '20', 'green', '4', '-0.215 -1.961 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText5', '23', 'green', '4', '1 -1.961 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText6', '16', 'green', '4', '-0.468 -2.374 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText7', '12', 'green', '4', '0.466 -2.374 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText8', '12', 'green', '4', '1.395 -2.374 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText9', '25', 'green', '4', '-1.171 -2.229 -5', 'planeValues', 'Arial_round_bold-msdf.json', 'false');

		elemCreate('0', 'ecamEngineDisplay', '0 0 0', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		elemCreate('0', 'engineSystemDisplay', '0 0 0', '0 0 0', '1 1 1', 'ecamEngineDisplay', 'a-entity');
		textCreate('val1ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val2ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val3ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val4ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val5ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val6ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val7ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val8ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val9ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val10ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val11ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val14ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val15ESD', '0', 'green', '5', '0 0 -10', 'engineSystemDisplay', 'Arial_round_bold-msdf.json', 'false');

		elemCreate('0', 'ecamPermanentDisplay', '3.169 0.814 0.041', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		textCreate('val1PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val2PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val3PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val4PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val5PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val6PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('val7PD', '0', 'green', '5', '0 0 -10', 'ecamPermanentDisplay', 'Arial_round_bold-msdf.json', 'false');

		elemCreate('0', 'ecamWarningDisplay', '0 -1 -5', '0 0 0', '1 1 1', 'ecam', 'a-entity');
		textCreate('stText10', '-  SENSED ABNORMAL AND EMERGENCY', 'yellow', '5', '4.792 0 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText11', '   PROCEDURES.', 'yellow', '5', '4.96 -0.253 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText12', '-  LIMITATIONS AND MEMOS', 'yellow', '5', '4.792 -0.698 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText13', '-  DEFERRED PROCEDURES', 'yellow', '5', '4.792 -1.110 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText14', '-  NOT SENSED ABNORMAL AND EMERGENCY', 'yellow', '5', '4.792 -1.524 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');
		textCreate('stText15', '   PROCEDURES', 'yellow', '5', '4.96 -1.777 0', 'ecamWarningDisplay', 'Arial_round_bold-msdf.json', 'false');

		imgCreate('models/ecam/mail.png', 'mail', '7.1 2.603 -4.9', '0 0 0', '0.015 0.015 0.015', 'ecam', 'a-image', '413', '143', 'true', '0');
	}
}

function ecamChange()
{
	var cabAltVal = temp29[i];
	temp29[i] = parseInt(temp29[i]);
	temp29[i] = ((temp29[i]/11000)*220) - 45;
	cabAlt.setAttribute('rotation', temp29[i] + ' 90 0');
	textChange(val4SD, temp29_pos, temp29_col, cabAltVal, temp29_size, 1);

	var cabVSVal = temp30[i];
	temp30[i] = parseInt(temp30[i]);
	temp30[i] = ((temp30[i]/65536)*180);
	cabVs.setAttribute('rotation', temp30[i] + ' 90 0');
	textChange(val5SD, temp30_pos, temp30_col, cabVSVal, temp30_size, 1);

	var delPVal = temp31[i];
	temp31[i] = parseInt(temp31[i]);
	temp31[i] = ((temp31[i]/10)*180) - 50;
	delP.setAttribute('rotation', temp31[i] + ' 90 0');
	textChange(val6SD, temp31_pos, temp31_col, delPVal, temp31_size, 1);

	var egtLVal = temp8[i];
	temp8[i] = parseInt(temp8[i]);
	temp8[i] = temp8[i] + 80;
	temp8[i] = ((temp8[i]/1342)*180);
	egtL.setAttribute('rotation', temp8[i] + ' 90 0');
	textChange(val8ESD, temp8_pos, temp8_col, egtLVal, temp8_size, 1);

	var egtRVal = temp9[i];
	temp9[i] = parseInt(temp9[i]);
	temp9[i] = temp9[i] + 80;
	temp9[i] = ((temp9[i]/1342)*180);
	egtR.setAttribute('rotation', temp9[i] + ' 90 0');
	textChange(val9ESD, temp9_pos, temp9_col, egtRVal, temp9_size, 1);

	thrust1.setAttribute('visible','true');
	var thrust1Plane = parseInt(temp10[i]);
	thra1.setAttribute('rotation', temp10[i] + ' 90 90');
	thra1plane1.setAttribute('rotation', -(90 + thrust1Plane) + ' 270 0');
	textChange(val10ESD, temp10_pos, temp10_col, temp10[i], temp10_size, 1);

	thrust2.setAttribute('visible','true');
	var thrust2Plane = parseInt(temp11[i]);
	thra2.setAttribute('rotation', temp11[i] + ' 90 90');
	thra2plane1.setAttribute('rotation', -(90 + thrust2Plane) + ' 270 0');
	textChange(val11ESD, temp11_pos, temp11_col, temp12[i], temp11_size, 1);

	var thrustPlane1 = parseInt(temp12[i]);
	thrc1.setAttribute('rotation', temp12[i] + ' 90 90');
	thra1plane2.setAttribute('rotation', (90 + thrustPlane1) + ' 90 0');

	var thrustPlane2 = parseInt(temp13[i]);
	thrc2.setAttribute('rotation', temp13[i] + ' 90 90');
	thra2plane2.setAttribute('rotation', (90 + thrustPlane2) + ' 90 0');

	textChange(val1ESD, temp1_pos, temp1_col, 'A. Floor', temp1_size, temp1[i]);
	textChange(val2ESD, temp2_pos, temp2_col, temp2[i], temp2_size, 1);
	textChange(val3ESD, temp3_pos, temp3_col, 'NAI', temp3_size, temp3[i]);
	textChange(val4ESD, temp4_pos, temp4_col, 'WAI', temp4_size, temp4[i]);
	textChange(val5ESD, temp5_pos, temp5_col, 'PACKS', temp5_size, temp5[i]);
	textChange(val6ESD, temp6_pos, temp6_col, 'IDLE', temp6_size, temp6[i]);
	textChange(val7ESD, temp7_pos, temp7_col, temp7[i], temp7_size, 1);
	textChange(val14ESD, temp14_pos, temp14_col, temp14[i], temp14_size, 1);
	textChange(val15ESD, temp15_pos, temp15_col, temp15[i], temp15_size, 1);

	textChange(val1PD, temp19_pos, temp19_col, temp19[i], temp19_size, 1);
	textChange(val2PD, temp20_pos, temp20_col, temp20[i], temp20_size, 1);
	textChange(val3PD, temp21_pos, temp21_col, temp21[i], temp21_size, 1);
	textChange(val4PD, temp22_pos, temp22_col, temp22[i], temp22_size, 1);
	textChange(val5PD, temp23_pos, temp23_col, temp23[i], temp23_size, 1);
	textChange(val6PD, temp24_pos, temp24_col, temp24[i], temp24_size, 1);
	textChange(val7PD, temp25_pos, temp25_col, temp25[i], temp25_size, 1);

	textChange(val1SD, temp26_pos, temp26_col, temp26[i], temp26_size, 1);
	textChange(val2SD, temp27_pos, temp27_col, temp27[i], temp27_size, 1);
	textChange(val3SD, temp28_pos, temp28_col, '1998', temp28_size, 1);
	textChange(val7SD, temp32_pos, temp32_col, temp32[i], temp32_size, 1);
	textChange(val8SD, temp33_pos, temp33_col, temp33[i], temp33_size, 1);

	if(temp51[i] != undefined)
	{
		var a = temp51[i].split(' ');
		temp51[i] = a[0];
		temp52[i] = a[1];
	}
}

function verticalSpeedAnimationVisualisation()
{
	temp62[i] = parseInt(temp62[i]);
	temp63[i] = parseInt(temp63[i]);

	if(temp62[i] > 200 || temp62[i] < -200)
	{
		digTextBox.setAttribute('visible','true');
	}

	if(temp62[i] < 200 && temp62[i] > -200)
	{
		greenVSpeed.setAttribute('visible','false');
		amberVSpeed.setAttribute('visible','false');
	}

	if(temp62[i] > 6000 || temp62[i] < -6000)
	{
		greenVSpeed.setAttribute('visible','false');
		amberVSpeed.setAttribute('visible','true');

		if(temp62[i] > 6000)
		{
			amberVSpeed.setAttribute('position','0.615 0.001 -0.310');
			amberVSpeed.setAttribute('rotation','0 15 0');

			digTextBox.setAttribute('position','0.615 0.001 -0.370');
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		if(temp62[i] < -6000)
		{
			amberVSpeed.setAttribute('position','0.53 0.001 0.39');

			amberVSpeed.setAttribute('rotation','0 -15 0');

			digTextBox.setAttribute('position','0.53 0.001 0.39');
			var valBox = temp62[i]/100;
			if(valBox <= -10)
			{
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}
	}

	else if(temp62[i] < -2000 && temp63[i] > 1000 && temp63[i] < 2500)
	{
		greenVSpeed.setAttribute('visible','false');
		amberVSpeed.setAttribute('visible','true');
	}

	else if(temp62[i] < -1200 && temp63[i] < 1000)
	{
		greenVSpeed.setAttribute('visible','false');
		amberVSpeed.setAttribute('visible','true');
	}

	else
	{
		greenVSpeed.setAttribute('visible','true');
		amberVSpeed.setAttribute('visible','false');
	}

	if(temp62[i] >= 0)
	{
		greenVSpeed.setAttribute('position','0.595 0.001 0');
		amberVSpeed.setAttribute('position','0.595 0.001 0');

		greenVSpeed.setAttribute('rotation','0 15 0');
		amberVSpeed.setAttribute('rotation','0 15 0');

		if(temp62[i] >= 0 && temp62[i] <= 200)
		{
			greenVSpeed.setAttribute('visible','false');
			amberVSpeed.setAttribute('visible','false');
			digTextBox.setAttribute('visible','false');
		}

		else if(temp62[i] > 200 && temp62[i] < 1000)
		{
			var tempPos = 1000 - temp62[i];
			tempPos = (tempPos/1000)*0.11 - 0.08 - 0.03;

			greenVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);

			var valPos = tempPos - 0.06 - 0.03;

			digTextBox.setAttribute('position','0.615 0.001 ' + valPos);
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] === 1000)
		{
			greenVSpeed.setAttribute('position','0.615 0.001 -0.190');
			amberVSpeed.setAttribute('position','0.615 0.001 -0.190');

			digTextBox.setAttribute('position','0.615 0.001 -0.250');
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] > 1000 && temp62[i] < 2000)
		{
			var tempPos = 2000 - temp62[i];
			tempPos = (tempPos/1000)*0.06;
			tempPos = (-0.16 - tempPos - 0.03);

			greenVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);

			var valPos = tempPos - 0.06 - 0.03;

			digTextBox.setAttribute('position','0.615 0.001 ' + valPos);
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] === 2000)
		{
			greenVSpeed.setAttribute('position','0.615 0.001 -0.250');
			amberVSpeed.setAttribute('position','0.615 0.001 -0.250');

			digTextBox.setAttribute('position','0.615 0.001 -0.310');
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] > 2000 && temp62[i] < 6000)
		{
			var tempPos = temp62[i] - 2000;
			tempPos = (tempPos/4000)*0.06;
			tempPos = (-0.22 - tempPos - 0.03);

			greenVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.615 0.001 ' + tempPos);

			var valPos = tempPos - 0.06 - 0.03;

			digTextBox.setAttribute('position','0.615 0.001 ' + valPos);
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] >= 6000)
		{
			greenVSpeed.setAttribute('position','0.615 0.001 -0.310');
			amberVSpeed.setAttribute('position','0.615 0.001 -0.310');

			digTextBox.setAttribute('position','0.615 0.001 -0.370');
			var valBox = temp62[i]/100;
			if(valBox >= 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}
	}

	else
	{
		greenVSpeed.setAttribute('position','0.595 0.001 0');
		amberVSpeed.setAttribute('position','0.595 0.001 0');

		greenVSpeed.setAttribute('rotation','0 -15 0');
		amberVSpeed.setAttribute('rotation','0 -15 0');

		if(temp62[i] <= 0 && temp62[i] >= -200)
		{
			greenVSpeed.setAttribute('visible','false');
			amberVSpeed.setAttribute('visible','false');
			digTextBox.setAttribute('visible','false');
		}

		else if(temp62[i] < -200 && temp62[i] > -1000)
		{
			var tempPos = temp62[i] - 1000;
			tempPos = (tempPos/1000)*0.27;
			tempPos = (tempPos * (-1) - 0.27);
			temp62[i] = parseInt(temp62[i]);

			greenVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);

			digTextBox.setAttribute('position','0.595 0.001 ' + tempPos);
			var valBox = temp62[i]/100;
			if(valBox > -10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] === -1000)
		{
			greenVSpeed.setAttribute('position','0.595 0.001 0.27');
			amberVSpeed.setAttribute('position','0.595 0.001 0.27');

			digTextBox.setAttribute('position','0.595 0.001 0.27');
			var valBox = temp62[i]/100;
			if(valBox <= -10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] < -1000 && temp62[i] > -2000)
		{
			var tempPos = temp62[i] + 2000;
			tempPos = (tempPos/1000)*0.06;
			tempPos = (0.36 - tempPos - 0.03);

			greenVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);

			digTextBox.setAttribute('position','0.595 0.001 ' + tempPos);
			var valBox = temp62[i]/100;
			if(valBox <= -10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] === -2000)
		{
			greenVSpeed.setAttribute('position','0.595 0.001 0.33');
			amberVSpeed.setAttribute('position','0.595 0.001 0.33');

			digTextBox.setAttribute('position','0.595 0.001 0.33');
			var valBox = temp62[i]/100;
			if(valBox <= -10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] < -2000 && temp62[i] > -6000)
		{
			var tempPos = temp62[i] + 6000;
			tempPos = (tempPos/4000)*0.06;
			tempPos = (0.42 - tempPos - 0.03);

			greenVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);
			amberVSpeed.setAttribute('position','0.595 0.001 ' + tempPos);

			digTextBox.setAttribute('position','0.595 0.001 ' + tempPos);
			var valBox = temp62[i]/100;
			if(valBox <=- 10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}

		else if(temp62[i] <= -6000)
		{
			greenVSpeed.setAttribute('position','0.595 0.001 0.39');
			amberVSpeed.setAttribute('position','0.595 0.001 0.39');

			digTextBox.setAttribute('position','0.595 0.001 0.39');
			var valBox = temp62[i]/100;
			if(valBox <= -10)
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value',valBox);
			}
			else
			{
				valBox = parseInt(valBox);
				digTextBox.setAttribute('value','0' + valBox);	
			}
		}
	}
}

function pitchAnimChange()
{
	temp86[i] = parseInt(temp86[i]);

	if(temp86[i] === 0)
	{
		pitchEqual.setAttribute('position','0 0 -0.03');
		pitchCross.setAttribute('position','0 0 -0.03');
	}

	if(temp86[i] === 10)
	{
		pitchEqual.setAttribute('position','0 0 0.052');
		pitchCross.setAttribute('position','0 0 0.052');
	}

	if(temp86[i] === 20)
	{
		pitchEqual.setAttribute('position','0 0 0.132');
		pitchCross.setAttribute('position','0 0 0.132');
	}

	if(temp86[i] === 30)
	{
		pitchEqual.setAttribute('position','0 0 0.2');
		pitchCross.setAttribute('position','0 0 0.2');
	}

	if(temp86[i] === 50)
	{
		pitchEqual.setAttribute('position','0 0 0.29');
		pitchCross.setAttribute('position','0 0 0.29');
	}

	if(temp86[i] === 80)
	{
		pitchEqual.setAttribute('position','0 0 0.43');
		pitchCross.setAttribute('position','0 0 0.43');
	}

	if(temp86[i] === -10)
	{
		pitchEqual.setAttribute('position','0 0 -0.11');
		pitchCross.setAttribute('position','0 0 -0.11');
	}

	if(temp86[i] === -20)
	{
		pitchEqual.setAttribute('position','0 0 -0.16');
		pitchCross.setAttribute('position','0 0 -0.16');
	}

	if(temp86[i] === -30)
	{
		pitchEqual.setAttribute('position','0 0 -0.2');
		pitchCross.setAttribute('position','0 0 -0.2');
	}

	if(temp86[i] === -50)
	{
		pitchEqual.setAttribute('position','0 0 -0.295');
		pitchCross.setAttribute('position','0 0 -0.295');
	}

	if(temp86[i] === -80)
	{
		pitchEqual.setAttribute('position','0 0 -0.43');
		pitchCross.setAttribute('position','0 0 -0.43');
	}
}

function airSpeedTargetChange()
{
	temp84[i] = parseInt(temp84[i]);
	var spPos = (temp84[i])/10;
	spPos = -2.075 + (0.069 * spPos);
	hSpeed.setAttribute('position','-0.420 0.001 ' + spPos);

	temp87[i] = parseInt(temp87[i]);
	var speedDiff = temp87[i] - temp84[i];
	speedDiff = speedDiff/10;
	if(speedDiff > 2.9 || speedDiff < -2.9)
	{
		targetM.setAttribute('visible','false');
		targetC.setAttribute('visible','false');

		if(speedDiff < 0)
		{
			targetDown.setAttribute('value',temp87[i]);
			if(temp88[i] === '1')
			{
				targetDown.setAttribute('color','#a7009f');
			}
			else
			{
				targetDown.setAttribute('color','#228cff');
			}
			targetDown.setAttribute('visible','true');
			targetUp.setAttribute('visible','false');
		}

		else
		{
			targetUp.setAttribute('value',temp87[i]);
			if(temp88[i] === '1')
			{
				targetUp.setAttribute('color','#a7009f');
			}
			else
			{
				targetUp.setAttribute('color','#228cff');
			}
			targetUp.setAttribute('visible','true');
			targetDown.setAttribute('visible','false');
		}
	}

	else
	{
		targetUp.setAttribute('visible','false');
		targetDown.setAttribute('visible','false');

		if(temp88[i] === '1')
		{
			targetM.setAttribute('visible','true');
			targetC.setAttribute('visible','false');
		}

		else
		{
			targetM.setAttribute('visible','false');
			targetC.setAttribute('visible','true');
		}

		if(speedDiff < 0)
		{
			var targetPos = speedDiff * 0.125;

			targetM.setAttribute('position','-0.335 0.006 ' + targetPos);
			targetC.setAttribute('position','-0.335 0.006 ' + targetPos);
		}

		else
		{
			var targetPos = speedDiff * 0.125 * -1;

			targetM.setAttribute('position','-0.335 0.006 ' + targetPos);
			targetC.setAttribute('position','-0.335 0.006 ' + targetPos);
		}
	}
}

function vmaxChange()
{
	temp84[i] = parseInt(temp84[i]);
	var vmaxDiff = temp92[i] - temp84[i];
	if(vmaxDiff > 0)
	{
		vmaxRed.setAttribute('visible','true');
	}
	vmaxDiff = vmaxDiff/10;
	var vmaxPos = (-0.37 - (vmaxDiff * 0.085))
	vmaxRed.setAttribute('position', '-0.342 0.003 ' + vmaxPos);

	if(temp65[i] === '1' || temp65[i] === 1 || temp66[i] === '1' || temp66[i] === 1)
	{
		vProtCross.setAttribute('visible','true');
		vProtEqual.setAttribute('visible','false');
	}

	else
	{
		vProtCross.setAttribute('visible','false');
		vProtEqual.setAttribute('visible','true');
	}
}

function vminChange()
{
	temp84[i] = parseInt(temp84[i]);
	var vminDiff = temp84[i] - temp93[i];
	if(vminDiff > 0)
	{
		vminAmberLine.setAttribute('visible','true');
	}
	vminDiff = vminDiff/10;
	var vminPos = (0.37 + (vminDiff * 0.085));
	vminAmberLine.setAttribute('position','-0.342 0.003 ' + vminPos);

	if(temp65[i] === '1' || temp65[i] === 1 || temp66[i] === '1' || temp66[i] === 1)
	{
		var vminAlDiff = temp84[i] - temp97[i];
		if(vminAlDiff > 0)
		{
			vminRed.setAttribute('visible','true');
		}
		vminAlDiff = vminAlDiff/10;
		var vminAlPos = (0.37 + (vminAlDiff * 0.085));
		vminRed.setAttribute('position','-0.342 0.004 ' + vminAlPos);
	}

	else
	{
		temp94[i] = parseInt(temp94[i]);
		var vminProtDiff = temp84[i] - temp94[i];
		if(vminProtDiff > 0)
		{
			vminAmber.setAttribute('visible','true');
		}
		vminProtDiff = vminProtDiff/10;
		var vminProtPos = (0.37 + (vminProtDiff * 0.085));
		vminAmber.setAttribute('position','-0.342 0.004 ' + vminProtPos);

		var vminAlDiff = temp84[i] - temp95[i];
		if(vminAlDiff > 0)
		{
			vminRedLine.setAttribute('visible','true');
		}
		vminAlDiff = vminAlDiff/10;
		var vminAlPos = (0.37 + (vminAlDiff * 0.085));
		vminRedLine.setAttribute('position','-0.342 0.005 ' + vminAlPos);
	}
}

function compassTargetChange()
{
	temp89[i] = parseInt(temp89[i])
	var cmpPos = temp89[i]/10;
	cmpPos = 1.930 - (0.105 * cmpPos);

	compassFoot.setAttribute('position', cmpPos + ' -0.018 0.5');

	temp90[i] = parseInt(temp90[i]);
	var tarDiff = temp90[i] - temp89[i];
	tarDiff = tarDiff/10;
	if(tarDiff > 0)
	{
		var tarPos = 0.105 * tarDiff;
		compassTarget.setAttribute('position', tarPos + ' 0.005 0.455');

		if(tarPos > 0.275)
		{
			compassTargetRight.setAttribute('visible','true');
			compassTargetRight.setAttribute('value',temp90[i]);
		}

		else
		{
			compassTargetRight.setAttribute('visible','false');
		}
	}

	else
	{
		var tarPos = 0.105 * tarDiff * -1;
		compassTarget.setAttribute('position', tarPos + ' 0.005 0.455');

		if(tarPos < -0.32)
		{
			compassTargetLeft.setAttribute('visible','true');
			compassTargetLeft.setAttribute('value',temp90[i]);
		}

		else
		{
			compassTargetLeft.setAttribute('visible','false');
		}
	}

	var curDiff = temp91[i] - temp89[i];
	curDiff = curDiff/10;
	if(curDiff > 0)
	{
		var curPos = 0.105 * curDiff;
		compassCurrent.setAttribute('position', curPos + ' 0.007 0.482');
	}

	else
	{
		var curPos = 0.105 * curDiff * -1;
		compassCurrent.setAttribute('position', curPos + ' 0.007 0.482');
	}
}

function verticalSpeedChange()
{
	boxHelpText.setAttribute('value', temp96[i]);
	boxHelpUp.setAttribute('value',temp96[i]);
	boxHelpDown.setAttribute('value',temp96[i]);

	temp96[i] = parseInt(temp96[i]);
	temp99[i] = parseInt(temp99[i]);

	var diffCheck = 0;
	var checkDiff = temp96[i] - temp99[i];

	var helpDiff = temp96[i] - temp99[i];
	helpDiff = helpDiff/500;
	var helpPos = 0.02 - (0.22 * helpDiff);

	if(checkDiff > 800)
	{
		boxHelp.setAttribute('visible','false');
		boxHelpUp.setAttribute('visible','true');
		diffCheck = 1;
	}

	else
	{
		boxHelp.setAttribute('position', '0.338 0.03 ' + helpPos);
		boxHelp.setAttribute('visible','true');
		boxHelpUp.setAttribute('visible','false');
	}

	if(checkDiff < -800)
	{
		boxHelp.setAttribute('visible','false');
		boxHelpDown.setAttribute('visible','true');
	}

	else
	{
		boxHelp.setAttribute('position', '0.338 0.03 ' + helpPos);
		boxHelp.setAttribute('visible','true');
		boxHelpDown.setAttribute('visible','false');
		if(diffCheck === 1)
		{
			boxHelp.setAttribute('visible','false');
			diffCheck = 0;
		}
	}

	if(temp74[i] === '1' || temp74[i] === 1 || temp75[i] === '1' || temp75[i] === 1)
	{
		var altVal = temp99[i]/100;

		if(altVal < 0)
		{
			negVal.setAttribute('visible','true');
			altVal = (-1)*altVal;
		}
		else
		{
			negVal.setAttribute('visible','false');	
		}

		altVal = parseInt(altVal);
		vDigSpeed.setAttribute('value',altVal);
		if(altVal < 10)
		{
			vDigSpeed.setAttribute('position','0.415 0.033 -0.027');
		}
		else if(altVal < 100 && altVal >= 10)
		{
			vDigSpeed.setAttribute('position','0.39 0.033 -0.027');
		}
		else if(altVal >= 100)
		{
			vDigSpeed.setAttribute('position','0.365 0.033 -0.027');	
		}

		temp99[i] = parseInt(temp99[i]);
		var spPos = temp99[i]/500;
		spPos = -9.75 + (0.225 * spPos);
		vSpeedScale.setAttribute('position','0.395 0.003 ' + spPos);

		temp95[i] = temp99[i]%100;

		if(temp95[i] === '00' || temp95[i] === '0')
		{
			vSpeedHelp.setAttribute('position','0.447 0.030 -0.073');
		}

		if(temp95[i] === '20')
		{
			vSpeedHelp.setAttribute('position','0.447 0.030 -0.042');
		}

		if(temp95[i] === '40')
		{
			vSpeedHelp.setAttribute('position','0.447 0.030 -0.016');
		}

		if(temp95[i] === '60')
		{
			vSpeedHelp.setAttribute('position','0.447 0.030 0.013');
		}

		if(temp95[i] === '80')
		{
			vSpeedHelp.setAttribute('position','0.447 0.030 0.041');
		}
	}

	else
	{
		var altVal = temp98[i]/100;

		if(altVal < 0)
		{
			negVal.setAttribute('visible','true');
			altVal = (-1)*altVal;
		}
		else
		{
			negVal.setAttribute('visible','false');	
		}

		altVal = parseInt(altVal);
		vDigSpeed.setAttribute('value',altVal);

		temp98[i] = parseInt(temp98[i]);
		var spPos = temp98[i]/500;
		spPos = -9.75 + (0.225 * spPos);
		vSpeedScale.setAttribute('position','0.395 0.003 ' + spPos);

		var changeSpeed = temp98[i]%100;

		if(changeSpeed === 0)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.073');
		}

		if(changeSpeed > 0 && changeSpeed < 20)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.057');
		}

		if(changeSpeed === 20)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.042');
		}

		if(changeSpeed > 20 && changeSpeed < 40)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.029');
		}

		if(changeSpeed === 40)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.016');
		}

		if(changeSpeed > 40 && changeSpeed < 60)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 -0.001');
		}

		if(changeSpeed === 60)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 0.013');
		}

		if(changeSpeed > 60 && changeSpeed < 80)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 0.027');
		}

		if(changeSpeed === 80)
		{
			vSpeedHelp.setAttribute('position','0.475 0.030 0.041');
		}
	}
}

function pfdChange()
{
	rollPoint.setAttribute('rotation', '0 ' + (-temp64[i]) + ' 0');
	rollTrapezoid.setAttribute('rotation', '0 ' + (-temp64[i] - temp71[i]) + ' 0');
	cyanTrapezoid.setAttribute('rotation', '0 ' + (-temp64[i] - temp71[i]) + ' 0');

	pitchEqual.setAttribute('rotation', '-90 ' + (-temp64[i] - temp71[i]) + ' 0');
	pitchCross.setAttribute('rotation', '-90 ' + (-temp64[i] - temp71[i]) + ' 0');

	textChange(val1PFDHD, temp51_pos, temp51_col, temp51[i], temp51_size, 1);
	textChange(val2PFDHD, temp52_pos, temp52_col, temp52[i], temp52_size, 1);
	textChange(val3PFDHD, temp53_pos, temp53_col, temp53[i], temp53_size, 1);
	textChange(val4PFDHD, temp54_pos, temp54_col, temp54[i], temp54_size, 1);
	textChange(val5PFDHD, temp55_pos, temp55_col, temp55[i], temp55_size, 1);
	textChange(val6PFDHD, temp56_pos, temp56_col, temp56[i], temp56_size, 1);
	textChange(val8PFDHD, temp58_pos, temp58_col, temp58[i], temp58_size, 1);
	textChange(val9PFDHD, temp59_pos, temp59_col, temp59[i], temp59_size, 1);
	textChange(val10PFDHD, temp60_pos, temp60_col, temp60[i], temp60_size, 1);
	textChange(val11PFDHD, temp61_pos, temp61_col, temp61[i], temp61_size, 1);

	if(temp72[i] < '.45')
	{
		textChange(machN, temp72_pos, temp72_col, temp72[i], temp72_size, 0);
	}
	else
	{
		textChange(machN, temp72_pos, temp72_col, temp72[i], temp72_size, 1);
	}

	if(temp79[i] === '1')
	{
		cyanTrapezoid.setAttribute('visible','true');
		rollTrapezoid.setAttribute('visible','false');
	}

	else
	{
		cyanTrapezoid.setAttribute('visible','false');
		rollTrapezoid.setAttribute('visible','true');
	}

	verticalSpeedAnimationVisualisation();

	if(temp65[i] === '1' || temp66[i] === '1')
	{
		bankAngleProtectionEqual.setAttribute('visible','false');
		bankAngleProtectionCross.setAttribute('visible','true');

		pitchEqual.setAttribute('visible','false');
		pitchCross.setAttribute('visible','true');
	}
	else
	{
		bankAngleProtectionEqual.setAttribute('visible','true');
		bankAngleProtectionCross.setAttribute('visible','false');

		pitchEqual.setAttribute('visible','true');
		pitchCross.setAttribute('visible','false');
	}

	if(temp80[i] === '1')
	{
		rollBar.setAttribute('visible','true');
		pitchBar.setAttribute('visible','true');
	}

	else
	{
		rollBar.setAttribute('visible','false');
		pitchBar.setAttribute('visible','false');
	}

	var pitchPos = (0.15/50)*temp81[i];
	var rollPos = (0.15/50)*temp82[i];

	rollBar.setAttribute('position', pitchPos + ' 0 0');
	pitchBar.setAttribute('position', '0 0 ' + rollPos);

	var orderPosX = (0.28/18)*temp69[i];
	var orderPosY = (0.28/20)*temp70[i];
	var yawPos = (0.28/50)*temp68[i];

	order.setAttribute('position', orderPosX + ' 0 ' + orderPosY);
	yaw.setAttribute('position', yawPos + ' 0 0.065');

	if(temp63[i] < '2500')
	{
		raHeight.setAttribute('visible','true');
		if(temp63[i] >= '400')
		{
			textChange(raHeight, '-0.057 0 0.240', 'green', temp63[i], '1.3', 1);
		}
		else
		{
			textChange(raHeight, '-0.057 0 0.240', '#807a25', temp63[i], '1.3', 1);
		}
	}

	else
	{
		raHeight.setAttribute('visible','false');
	}

	if(temp73[i] === '1')
	{
		sqfe.setAttribute('visible','false');
		sqfeVal.setAttribute('visible','false');
	}

	if(temp74[i] === '1')
	{
		std.setAttribute('visible','false');
		sqfe.setAttribute('value','QNH');
	}

	if(temp75[i] === '1')
	{
		std.setAttribute('visible','false');
		sqfe.setAttribute('value','QFE');
	}

	if(temp76[i] === '1')
	{
		std.setAttribute('visible','false');
		sqfeVal.setAttribute('value',temp78[i]);
	}

	else
	{
		std.setAttribute('visible','false');
		sqfeVal.setAttribute('value',temp77[i]);	
	}

	if(temp83[i] === '1')
	{
		trueCom.setAttribute('visible','true');
		magCom.setAttribute('visible','false');
		hdgCom.setAttribute('visible','false');
	}

	else if(temp83[i] === '0')
	{
		trueCom.setAttribute('visible','false');
		magCom.setAttribute('visible','true');
		hdgCom.setAttribute('visible','false');
	}

	else
	{
		trueCom.setAttribute('visible','false');
		magCom.setAttribute('visible','false');
		hdgCom.setAttribute('visible','true');
	}

	temp84[i] = parseInt(temp84[i]);
	temp85[i] = parseInt(temp85[i]);
	var hsDiff = temp84[i] - temp85[i];
	hsDiff = hsDiff/10;
	if(temp85[i] < 1 || hsDiff > 4 || hsDiff < -4)
	{
		lineTrend.setAttribute('visible','false');
	}

	else
	{
		lineTrend.setAttribute('visible','true');

		if(hsDiff < 0)
		{
			lineTrend.setAttribute('rotation','0 180 0');

			var scaleLine = hsDiff * 0.01 * -1;
			lineTrend.setAttribute('scale','0.01 0.01 ' + scaleLine);
			var arrowLine = 0.05/(scaleLine/0.01);
			arrowTrend.setAttribute('scale', '0.05 ' + arrowLine + ' 0.05');
		}

		else
		{
			lineTrend.setAttribute('rotation','0 0 0');

			var scaleLine = hsDiff * 0.01;
			lineTrend.setAttribute('scale','0.01 0.01 ' + scaleLine);
		}
	}

	pitchAnimChange();
	airSpeedTargetChange();
	compassTargetChange();
	verticalSpeedChange();
	vmaxChange();
	vminChange();
}

function changeAttributes(ob, pos, rot, sca)
{
	ob.setAttribute('position', pos);
	ob.setAttribute('rotation', rot);
	ob.setAttribute('scale', sca);
}

function timeAnimate()
{
	if(animStatus === true)
	{
		if(i < fileCount)
		{
			setTimeout(function()
			{
				ecamChange();
				pfdChange();

				i++;
				console.log(i);
				timeAnimate();
			}, timeVal);
		}
	}
}